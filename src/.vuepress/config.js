module.exports = {
	title: 'Meltano',
	description: 'From data source to dashboard',
	dest: './public',
	themeConfig: {
		nav: [
			{ text: 'Home', link: '/' },
			{ text: 'Guide', link: '/guide/' },
			{ text: 'Documentation', link: '/docs/' }
		],
		sidebar: {
			'/docs': [
				'/docs/',
				'/docs/source-to-dashboard',
				'/docs/version-control',
				'/docs/taps-targets',
				'/docs/contributing',
				'/docs/security-privacy',
				'/docs/resources',
				'/docs/license'
			]
		},
		nav: [
			{ text: 'Home', link: '/' },
			{ text: 'Guide', link: '/guide/' },
			{ text: 'Documentation', link: '/docs/' },
			{ text: 'About', link: '/about/' }
		],
		repo: 'https://gitlab.com/meltano/meltano',
		repoLabel: 'Repo',
		lastUpdated: 'Last Updated',
		docsDir: 'src',
		docsRepo: 'https://gitlab.com/meltano/meltano.com',
		editLinks: true,
		editLinkText: 'Help us improve this page!'
	}
}
