---
home: true
heroImage: /meltano-logo.svg
actionText: Get Started →
actionLink: /guide/
features:
  - title: Simplicity
    details: Make it easy for everyone  to make informed decisions about their business.
  - title: Savings
    details: Reduce time and cost for businesses that need to invest in data analytics.
  - title: Structure
    details: Allow  data scientists and analysts to build a robust, well-tested, and well-documented infrastructure that follows DevOps and data engineering best practices.

footer: Meltano is a GitLab startup | MIT Licensed | Copyright © 2018-Present
---
