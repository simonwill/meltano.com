---
title: "Machine Learning"
---

# Machine Learning

Meltano will start with [scikit](https://github.com/scikit-learn/scikit-learn), as a base, and may support other ML libraries in the future. Meltano will run the DBT transforms. Then we will run ML models against the DBT transforms which will result in new data in the DB.
Meltano will help with training the model:

* Splitting the data into the train, validation and tests steps.
* More to be defined.